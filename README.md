# Saeculum Node.js Assessment

## Description

This project is part of the Saeculum Node.js assessment, evaluating proficiency in Node.js/TypeScript and backend engineering. It consists of two tasks: User API and Appointment API.

## Task 1: User API

- Admin can list all user details with pagination.
- Admin can search users by full name, email, and status.
- Admin can create users with server-side and DB-side validations.
- Admin can update user status.

## Task 2: Appointment API

- Admin can list all appointment details with pagination.
- Admin can search appointments by user full name, user status, and appointment datetime.
- Admin can create appointments for any user with server-side and DB-side validations.

### Validations

1. Prevent conflicting appointments.
2. Allow multiple appointments per user per day.
3. Exclude inactive users from consideration.
4. Provide clear error messages.
5. Validate appointment start and end times.

## How to Run

1. npm install
2. node start

# API Documentation

# User API

GET /users/getAllUsers: Get all users with pagination.
POST /users/createUsers: Create a new user.
PUT /users/updateUsers/:id: Update user status.

# Appointment API

POST /appointments: Get all appointments with pagination and search.
POST /appointments/create: Create a new appointment.

Developed by Piyush

Thank you!