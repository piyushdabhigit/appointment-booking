// services/appointment.service.js
import Appointment from '../models/appointment.js';
import User from '../models/user.js';

const AppointmentService = {
  async createAppointment(appointmentData) {
    try {
      // Validate if user is active
      const user = await User.findById(appointmentData.userId);
      if (!user || user.status !== 'active') {
        throw new Error('User is inactive or does not exist');
      }

      // Validate if appointment time is in the future and not overlapping
      const currentTime = new Date();
      if (new Date(appointmentData.dateTime) <= currentTime) {
        throw new Error('Appointment time must be in the future');
      }

      // Check if there's already an appointment at the same time
      const existingAppointment = await Appointment.findOne({ dateTime: appointmentData.dateTime });
      if (existingAppointment) {
        throw new Error('Time slot not available');
      }

      // Validate if user has already booked an appointment for the same day
      const startOfDay = new Date(new Date(appointmentData.dateTime).setHours(0, 0, 0, 0));
      const endOfDay = new Date(new Date(appointmentData.dateTime).setHours(23, 59, 59, 999));
      const userAppointments = await Appointment.find({ 
        userId: appointmentData.userId,
        dateTime: { $gte: startOfDay, $lte: endOfDay }
      });
      if (userAppointments.length > 0) {
        throw new Error('User can only book one appointment per day');
      }

      // Validate if the end time is within 1 hour of the start time
      const endTime = new Date(appointmentData.dateTime);
      endTime.setHours(endTime.getHours() + 1);
      if (new Date(appointmentData.endDateTime) > endTime) {
        throw new Error('End time must be within 1 hour from start time');
      }

      // Create the appointment if all validations pass
      const appointment = new Appointment(appointmentData);
      await appointment.save();
      return appointment;
    } catch (err) {
      throw new Error(err.message);
    }
  },

  async getAllAppointments(page, limit) {
    try {
      const options = {
        page: parseInt(page) || 1,
        limit: parseInt(limit) || 10,
      };
      const totalCount = await Appointment.countDocuments();
      const appointments = await Appointment.find()
        .skip((options.page - 1) * options.limit)
        .limit(options.limit)
        .populate('userId');
  
      return {
        totalCount,
        totalPages: Math.ceil(totalCount / options.limit),
        currentPage: options.page,
        appointments,
      };
    } catch (err) {
      throw new Error(err.message);
    }
  },
  

  async searchAppointments(searchText) {
    try {
      const appointments = await Appointment.find({
        $or: [
          { 'userId.fullName': { $regex: searchText, $options: 'i' } },
          { 'userId.status': { $regex: searchText, $options: 'i' } },
          { dateTime: { $regex: searchText, $options: 'i' } }
        ]
      }).populate('userId');
      return successResponse(appointments);
    } catch (err) {
      return errorResponse(err.message);
    }
  },
};

export default AppointmentService;
