import User from '../models/user.js'; 
const UserService = {
  async getAllUsers(page, limit, searchText) {
    try {
      const query = {};
      if (searchText) {
        query.$or = [
          { fullName: { $regex: searchText, $options: 'i' } },
          { email: { $regex: searchText, $options: 'i' } },
          { status: { $regex: searchText, $options: 'i' } }
        ];
      }  
      const users = await User.find(query)
        .skip((page - 1) * limit)
        .limit(limit);
      return users;
    } catch (err) {
      throw new Error(err.message);
    }
  },

  async createUser(userData) {
    try {
      console.log('test');
      // Check if email already exists
      const existingUser = await User.findOne({ email: userData.email });
      console.log(existingUser);
      if (existingUser) {
        throw new Error('Email already exists');
      }

      const user = new User(userData);
      await user.save();
      return user;
    } catch (err) {
      throw new Error(err.message);
    }
  },

  async updateUserStatus(userId, status) {
    try {
      const updatedUser = await User.findByIdAndUpdate(userId, { status }, { new: true });
      if (!updatedUser) {
        throw new Error('User not found');
      }
      return updatedUser;
    } catch (err) {
      throw new Error(err.message);
    }
  }
};

export default UserService;
