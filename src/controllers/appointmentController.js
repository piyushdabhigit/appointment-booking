// controllers/appointment.controller.js
import { validationResult } from 'express-validator';
import appointmentService from '../service/appointmentService.js';
import { successResponse, errorResponse } from '../util/responseBuilder.js';

const AppointmentController = {
  async getAllAppointments(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errorResponse(errors.array()[0].msg));
    }

    try {
      const { page, limit, searchText } = req.body;

      if (searchText) {
        const appointments = await appointmentService.searchAppointments(searchText);
        res.json(successResponse(appointments));
      } else {
        const appointments = await appointmentService.getAllAppointments(page, limit);
        res.json(successResponse(appointments));
      }
    } catch (err) {
      res.status(500).json(errorResponse(err.message));
    }
  },

  async createAppointment(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errorResponse(errors.array()[0].msg));
    }

    try {
      const appointment = await appointmentService.createAppointment(req.body);
      res.status(201).json(successResponse(appointment, 'Appointment created successfully'));
    } catch (err) {
      res.status(400).json(errorResponse(err.message));
    }
  },
 
};

export default AppointmentController;
