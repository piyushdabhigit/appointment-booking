import { validationResult } from 'express-validator';
import userService from '../service/userService.js';
import { successResponse, errorResponse } from '../util/responseBuilder.js';

const UserController = {
  async getAllUsers(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errorResponse(errors.array()[0].msg));
    }

    try {
      const page = parseInt(req.body.page) || 1;
      const limit = parseInt(req.body.limit) || 10; 
      const searchText = req.body.searchText || '';

      const users = await userService.getAllUsers(page, limit, searchText);
      res.json(successResponse(users));
    } catch (err) {
      res.status(500).json(errorResponse(err.message));
    }
  },

  async createUser(req, res) {
    console.log('test');
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errorResponse(errors.array()[0].msg));
    }

    try {
      
      const user = await userService.createUser(req.body);
      console.log(user);
      res.status(201).json(successResponse(user, 'User created successfully'));
    } catch (err) {
      // Check if the error message is due to existing email
      if (err.message === 'Email already exists') {
        return res.status(400).json(errorResponse('Email already exists', 400));
      }
      res.status(400).json(errorResponse(err.message));
    }
  },

  async updateUserStatus(req, res) {
    try {
      const { id } = req.params;
      const { status } = req.body;
console.log(id,status);
      if (!['active', 'inactive'].includes(status)) {
        return res.status(400).json(errorResponse('Invalid status value', 400));
      }

      const updatedUser = await userService.updateUserStatus(id, status);
      res.json(successResponse(updatedUser, 'User status updated successfully'));
    } catch (err) {
      res.status(400).json(errorResponse(err.message));
    }
  }
};

export default UserController;
