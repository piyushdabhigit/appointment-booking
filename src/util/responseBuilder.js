const successResponse = (data = null, message = 'Success') => {
    return {
      success: true,
      message,
      data
    };
  };
  
  const errorResponse = (message = 'Internal Server Error', statusCode = 500) => {
    return {
      success: false,
      message,
      statusCode
    };
  };
  
  export { successResponse, errorResponse };
  