// routes/appointment.route.js
import express from 'express';
import appointmentController from '../controllers/appointmentController.js';
import { body } from 'express-validator';

const router = express.Router();

router.post('/appointments', [
  // Validation for searching appointments
  body('page').optional().isInt().withMessage('Page must be an integer'),
  body('limit').optional().isInt().withMessage('Limit must be an integer'),
  body('searchText').optional().trim().escape().notEmpty().withMessage('Search text cannot be empty')
], appointmentController.getAllAppointments);

router.post('/appointments/create', [
  // Validation for creating appointment
  body('userId').notEmpty().withMessage('User ID is required'),
  body('dateTime').notEmpty().withMessage('Appointment date and time is required')
], appointmentController.createAppointment);

export default router;
