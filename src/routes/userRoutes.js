// src/routes/user.route.js
import express from 'express';
import UserController from '../controllers/userController.js';
const router = express.Router();
import { body } from 'express-validator';

router.post('/users/getAllUsers', [
    body('page').optional().isInt().withMessage('Page must be an integer'),
    body('limit').optional().isInt().withMessage('Limit must be an integer'),
    body('searchText').optional().trim().escape().notEmpty().withMessage('Search text cannot be empty')
  ], UserController.getAllUsers);
  
router.post('/users/createUsers', UserController.createUser);
router.put('/users/updateUsers/:id', UserController.updateUserStatus);

export default router;
