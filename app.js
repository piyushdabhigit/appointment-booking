// index.js
import express from 'express';
import connectDB from './src/config/database.js';
import userRoutes from './src/routes/userRoutes.js';
import appointmentRoutes from './src/routes/appointmentRoutes.js';
import dotenv from 'dotenv'; 
dotenv.config();


const app = express();

// Connect to MongoDB
connectDB();

// Middleware
app.use(express.json());

// Routes
app.use('/api', userRoutes);
app.use('/api', appointmentRoutes);


// Start server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
